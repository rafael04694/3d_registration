# 3D Registration Pipeline

The objective of this assignment is to develop a registration pipeline to process images and point clouds captured by a UAV, and subsequently, generate a textured and denser representation of an offshore structure. 

## Challenges in this assignment:

1. Data Integration: Work with a combination of images and point clouds. Integrating these diverse data sources into a unified pipeline can be complex.

2. Point Cloud Texturization: one of the primary objectives is to apply texturing to the point cloud, giving it a realistic appearance. This involves mapping images onto the 3D structure, requiring careful alignment and blending of 2D images onto 3D surfaces.

3. Point Cloud Registration: to create a denser representation, you need to register multiple individual point clouds into a cohesive and complete 3D model. This involves aligning and merging point clouds from different viewpoints.

## Final Result

The final result was a functional 3d registration pipeline that was able to texturize each point cloud (fusing the this data with images), and then align all the point clouds to obtain a more realistic pc. The pipeline was able to achieve very good results using only 12 of the 100s of image+pointcloud pairs.
